# NeurIPS 2018 paper
Revision of my paper <a href="https://arxiv.org/abs/1709.08535">Analytic solution and stationary phase approximation for the Bayesian lasso and elastic net</a> 
from arXiv version v1 to versions v2 and v3.
